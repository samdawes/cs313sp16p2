Sam Dawes

TestIterator:

TODO also try with a LinkedList - does it make any difference?
— If you try it with a LinkedList instead of ArrayList nothing will change because the ArrayList methods (.add(), .next(), etc) are written and used the same way in both data structures.

TODO what happens if you use list.remove(77)?
— list.remove(77) will remove the number in the 77th index. It doesn’t remove the number 77 from the list. so this command wouldn’t work.

TestList:

TODO also try with a LinkedList - does it make any difference?
— It does not make a difference for the same reason as before.


Running Times:

testLinkedListAddRemove():
Size:		Time:
10:		39 ms
100:		44 ms
1000:		41 ms
10000:		39 ms

testLinkedListAccess():
Size:		Time:
10:		10 ms
100:		27 ms
1000:		375 ms
10000:		5563 ms

testArrayListAddRemove():
Size:		Time:
10:		68 ms
100:		40 ms
1000:		158 ms
10000:		1613 ms

testArrayListAccess():
Size:		Time:
10:		10 ms
100:		11 ms
1000:		11 ms
10000:		9 ms

LinkedList performs better at adding and removing while ArrayList is better at accessing.